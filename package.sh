#!/bin/bash

npm ci
npm run build

rm -rf node_modules
npm ci --production
rm -rf node_modules/.bin

# Enable recursive search for lib/**/*.js
shopt -s globstar

shasum --algorithm 256 package.json manifest.json lib/**/*.js LICENSE README.md CHANGELOG.md > SHA256SUMS
find node_modules -type f -exec shasum --algorithm 256 {} \; >> SHA256SUMS

TARFILE=`npm pack`
tar xzf "${TARFILE}"
cp -r node_modules ./package
tar czf "${TARFILE}" --sort=name --owner=root:0 --group=root:0 --mtime='UTC 2020-01-01' package

shasum --algorithm 256 "${TARFILE}" > "${TARFILE}".sha256sum

rm SHA256SUMS
rm -rf package
