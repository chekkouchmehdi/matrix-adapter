/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

import { MatrixAdapter } from './matrix-adapter';
import MatrixWrapper from './matrix-wrapper';

export = (addonManager: any, manifest: any) => {
  const matrixWrapper = new MatrixWrapper(manifest.moziot.config);

  new MatrixAdapter(addonManager, manifest, matrixWrapper);

  try {
    const { MatrixNotifier } = require('./matrix-notifier');
    new MatrixNotifier(addonManager, manifest, matrixWrapper);
  } catch (e) {
    console.error(e);
  }
}
