/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

import { Notifier, Outlet, Constants } from 'gateway-addon';
import MatrixWrapper from './matrix-wrapper';

class MatrixOutlet extends Outlet {
  constructor(notifier: Notifier, private config: any, private matrixWrapper: MatrixWrapper) {
    super(notifier, MatrixOutlet.name);
    this.name = 'Matrix';
    this.config = config;
  }

  async notify(_title: string, message: string, level: Constants.NotificationLevel) {
    await this.matrixWrapper.sendText({
      isNotice: level === Constants.NotificationLevel.LOW,
      message: level === Constants.NotificationLevel.HIGH ? `@room ${message}` : message,
      roomAliasOrId: this.config.room_id,
    });
  }
}

export class MatrixNotifier extends Notifier {
  constructor(addonManager: any, manifest: any, matrixWrapper: MatrixWrapper) {
    super(addonManager, MatrixNotifier.name, manifest.name);

    // Only initialize the Notifier, if a roomID has been provided.
    if (typeof manifest.moziot.config['room_id'] !== 'string') {
      return;
    }

    addonManager.addNotifier(this);

    if (!this.outlets[MatrixNotifier.name]) {
      this.handleOutletAdded(
        new MatrixOutlet(this, manifest.moziot.config, matrixWrapper)
      );
    }
  }
}
