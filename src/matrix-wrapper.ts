/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

import {
    LogLevel,
    LogService,
    MatrixClient,
    AutojoinRoomsMixin,
    AutojoinUpgradedRoomsMixin,
} from 'matrix-bot-sdk';
LogService.setLevel(LogLevel.ERROR);

export default class MatrixWrapper {
    private client: MatrixClient;

    constructor(private config: any) {
        if (typeof config !== 'object') {
            throw new Error('No config provided!');
        }
        if (typeof config['homeserver_url'] !== 'string') {
            throw new Error('homeserver_url is a required config field!');
        }
        if (typeof config['access_token'] !== 'string') {
            throw new Error('access_token is a required config field!');
        }

        this.client = new MatrixClient(
            this.config.homeserver_url,
            this.config.access_token,
        );
        if (config.autojoin_rooms === true) {
            AutojoinRoomsMixin.setupOnClient(this.client);
        }
        if (config.autojoin_upgraded_rooms === true) {
            AutojoinUpgradedRoomsMixin.setupOnClient(this.client);
        }
    }

    async sendText({ isNotice, message, roomAliasOrId }: { isNotice?: boolean, message: string, roomAliasOrId: string }) {
        const roomId = roomAliasOrId.startsWith('#')
            ? (await this.client.lookupRoomAlias(roomAliasOrId)).roomId
            : roomAliasOrId;

        console.log(`Sending post (room: ${roomId}): ${message}`);
        if (isNotice === true) {
            await this.client.sendNotice(roomId, message);
        } else {
            await this.client.sendText(roomId, message);
        }
    }
}
