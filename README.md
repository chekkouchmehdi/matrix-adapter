# Matrix Adapter

[![MPL 2.0 License](https://img.shields.io/badge/license-MPL--2.0-blue.svg)](LICENSE)

Send Matrix chat messages from your [Mozilla IoT gateway](https://iot.mozilla.org/gateway/).

![](./docs/post-action.png)

![](./docs/riot-chat-preview.png)

## Configuration
1. Go to a Matrix home server and register an account for the bot.
2. (Optional) Setup pantalaimon for the bot to support encryption.
3. Install the addon "Matrix chat" in your Mozilla IoT gateway.
4. Open the configuration of this addon and fill in the required information. You can find this information in your Matrix client, usually in the advanced settings.
5. Go to "Things" in you gateway and add the new Matrix device.

## Usage
Go to the settings to configure your homeserver and access token.

The addon registers a Matrix device with a `post(roomId, message, isNotice)` action.

Currently, a rule can only trigger parameterless actions. To send Matrix messages from a rule, you have to register a predefined message. Go to the settings of the addon and add a rule with a name and a message of your choice. The Matrix device now provides a new action with the specified name you can use in a rule.

The notifier's notification levels are:
* `Low` - Posts a notice
* `Normal` - Posts a regular message
* `High` - Notifies everyone in the room

## More screenshots

![](./docs/rules-preview.png)

![](./docs/config.png)
